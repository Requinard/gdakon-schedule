import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import { VitePWA } from "vite-plugin-pwa";
import { checker } from "vite-plugin-checker";
import Icons from "unplugin-icons/vite";
import { imagetools } from "vite-imagetools";
import AutoImport from "unplugin-auto-import/vite";
import tsconfigPaths from "vite-tsconfig-paths";
import Info from "unplugin-info/vite";
import { TanStackRouterVite } from "@tanstack/router-plugin/vite";
import i18nextLoader from "vite-plugin-i18next-loader";
// https://vitejs.dev/config/
export default defineConfig(({ command }) => {
    return {
        optimizeDeps: {
            exclude: command === "build" ? ["lodash"] : [],
        },
        build: {
            rollupOptions: {
                output: {
                    manualChunks: {
                        mui: ["@mui/material"],
                        mui_pickers: ["@mui/x-date-pickers"],
                        react: ["react", "react-dom", "react-router-dom"],
                        lodash: ["lodash"],
                    },
                },
            },
        },
        plugins: [
            AutoImport({
                imports: [
                    "react",
                    "react-i18next",
                    "ahooks",
                    {
                        imports: ["z"],
                        from: "zod",
                        type: true,
                    },
                    {
                        imports: [
                            "useFileRoute",
                            "getRouteInfo",
                            "useSearch",
                            "useNavigate",
                        ],
                        from: "@tanstack/react-router",
                    },
                    {
                        imports: ["useQuery", "useSuspenseQuery"],
                        from: "@tanstack/react-query",
                    },
                    {
                        imports: [
                            "Divider",
                            "Box",
                            "Card",
                            "CardHeader",
                            "Icon",
                            "Stack",
                            "Collapse",
                            "Fade",
                            "List",
                            "ListItem",
                            "ListItemText",
                            "ListItemButton",
                            "ListItemIcon",
                            "MenuItem",
                            "Container",
                            "Collapse",
                            "Typography",
                        ],
                        from: "@mui/material",
                    },
                    {
                        imports: ["match", "P"],
                        from: "ts-pattern",
                    },
                ],
                dts: "./src/auto-import.d.ts",
                eslintrc: {
                    enabled: true,
                },
            }),
            TanStackRouterVite({ autoCodeSplitting: true }),
            Icons({
                compiler: "jsx",
                jsx: "react",
                defaultClass: "iconified",
            }),
            react(),
            i18nextLoader({
                paths: ["./src/i18n/locales"],
                namespaceResolution: "relativePath",
            }),

            checker({
                typescript: true,
                overlay: {
                    initialIsOpen: false,
                    position: "br",
                },
                eslint: {
                    useFlatConfig: true,
                    lintCommand: "eslint ./src",
                },
                terminal: true,
            }),
            VitePWA({
                registerType: "autoUpdate",
                manifest: {
                    name: "Gdakon Pocket Schedule",
                    short_name: "Gdakon",
                    description: "Check the Gdakon schedule from your phone!",
                    lang: "en",
                    theme_color: "#3a3d80",
                    icons: [
                        {
                            src: "https://gdakon.org/favicon.ico",
                            sizes: "16x16",
                            type: "image/icon",
                        },
                    ],
                },
                workbox: {
                    globPatterns: [
                        "**/*.{js,css,html,ico,png,svg,json,webp,woff,woff2,ico}",
                    ],
                    cleanupOutdatedCaches: true,
                    sourcemap: true,
                },
            }),
            imagetools(),
            tsconfigPaths(),
            Info(),
        ],
        test: {
            passWithNoTests: true,
            environment: "happydom",
        },
    };
});
