import { createFileRoute } from "@tanstack/react-router";

import { AboutCard } from "../components/About/AboutCard";

import SettingsIcon from "~icons/mdi/settings";
import { ContainerWithBackground } from "~modules/Common";

export const Route = createFileRoute("/settings")({
    component: () => {
        return (
            <ContainerWithBackground sx={{ pt: 2 }}>
                <AboutCard />
            </ContainerWithBackground>
        );
    },
    staticData: {
        name: "Settings",
        icon: SettingsIcon,
    },
});
