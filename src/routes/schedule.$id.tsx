import { createFileRoute, notFound } from "@tanstack/react-router";
import { Dialog } from "@mui/material";
import { EventCard } from "../modules/Schedule/components/EventCard/EventCard";
import { scheduleQueryOptions } from "./index";
import { EventScheduleItemModel } from "~modules/Schedule";

export const Route = createFileRoute("/schedule/$id")({
    loader: async ({ context, params }) => {
        const result =
            await context.queryClient.ensureQueryData(scheduleQueryOptions);

        const events = result.map((it) => EventScheduleItemModel.parse(it));

        const item = events.find((it) => it.id === params.id);
        if (item === undefined) {
            throw notFound({});
        }
        return { event: item, events: events };
    },
    component: RouteComponent,
});

function RouteComponent() {
    const navigate = Route.useNavigate();
    const { event } = Route.useLoaderData();
    return (
        <Dialog
            maxWidth={"md"}
            open
            onClose={() =>
                navigate({
                    to: "/schedule",
                    search: (prev) => prev,
                })
            }
        >
            <EventCard event={event} />
        </Dialog>
    );
}
