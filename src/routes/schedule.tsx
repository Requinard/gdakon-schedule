import { createFileRoute, Outlet } from "@tanstack/react-router";
import { useSuspenseQuery } from "@tanstack/react-query";
import { zodValidator } from "@tanstack/zod-adapter";
import { z } from "zod";
import { useTheme } from "@mui/material";
import { useMemo } from "react";
import { useEventSearch } from "../modules/Schedule/hooks/useEventSearch";
import { scheduleQueryOptions } from "./index";
import { EventScheduleList, EventScheduleSearch } from "~modules/Schedule";

import SettingsIcon from "~icons/mdi/calendar-blank-multiple";
import { ContainerWithBackground } from "~modules/Common";

const scheduleSearchSchema = z.object({
    query: z.string().optional(),
    bookmarked: z.boolean().optional(),
    sort: z
        .enum(["date", "upcoming", "bookmarked", "room"])
        .default("upcoming"),
    date: z.string().optional(),
    room: z.string().optional(),
});
export const Route = createFileRoute("/schedule")({
    component: () => {
        const { data } = useSuspenseQuery(scheduleQueryOptions);
        const visibleEvents = useEventSearch(data);
        const theme = useTheme();
        const bgColor = useMemo(() => {
            return theme.palette.background.default
                .replace("rbg", "rgba")
                .replace(")", ",0.7)");
        }, [theme]);

        return (
            <ContainerWithBackground
                sx={{
                    backgroundColor: bgColor,
                    my: 2,
                    borderRadius: theme.spacing(1),
                    boxShadow: theme.shadows[7],
                }}
            >
                <Outlet />
                <EventScheduleSearch events={data} />

                <EventScheduleList events={data} visible={visibleEvents} />
            </ContainerWithBackground>
        );
    },
    loader: ({ context }) =>
        context.queryClient.ensureQueryData(scheduleQueryOptions),
    validateSearch: zodValidator(scheduleSearchSchema),
    staticData: {
        name: "Schedule",
        icon: SettingsIcon,
    },
});
