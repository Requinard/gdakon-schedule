import { createFileRoute } from "@tanstack/react-router";
import { useSuspenseQuery } from "@tanstack/react-query";

import { scheduleQueryOptions } from "./index";

import { PresentationMode } from "~modules/Schedule";
import PresentationIcon from "~icons/mdi/presentation";

export const Route = createFileRoute("/presentation")({
    loader: ({ context }) =>
        context.queryClient.ensureQueryData(scheduleQueryOptions),
    component: () => {
        const { data } = useSuspenseQuery(scheduleQueryOptions);

        return <PresentationMode events={data} />;
    },
    staticData: {
        name: "Presentation",
        icon: PresentationIcon,
        hideNavbar: true,
    },
});
