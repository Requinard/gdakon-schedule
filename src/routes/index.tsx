import { createFileRoute } from "@tanstack/react-router";
import { queryOptions, useQuery } from "@tanstack/react-query";

import { HomeRoute } from "../components";

import HomeIcon from "~icons/mdi/home";
import { EventScheduleItemModel, scheduleClient } from "~modules/Schedule";

export const scheduleQueryOptions = queryOptions({
    queryKey: ["schedule"],
    queryFn: scheduleClient.getSchedule,
    select: (data) => data.map((it) => EventScheduleItemModel.parse(it)),
});
export const Route = createFileRoute("/")({
    component: () => {
        useQuery(scheduleQueryOptions);

        return <HomeRoute />;
    },
    loader: ({ context }) =>
        context.queryClient.ensureQueryData(scheduleQueryOptions),

    staticData: {
        icon: HomeIcon,
        name: "Home",
    },
});
