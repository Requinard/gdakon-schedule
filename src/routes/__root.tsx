import { createRootRouteWithContext, Outlet } from "@tanstack/react-router";
import { QueryClient } from "@tanstack/react-query";
import { TanStackRouterDevtools } from "@tanstack/router-devtools";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";

import { Dashboard } from "~modules/Dashboard";

type RootContextType = {
    queryClient: QueryClient;
};
export const Route = createRootRouteWithContext<RootContextType>()({
    component: () => (
        <>
            <Dashboard>
                <Outlet />
            </Dashboard>
            {import.meta.env.DEV && (
                <>
                    <TanStackRouterDevtools position={"top-left"} />
                    <ReactQueryDevtools
                        buttonPosition={"top-right"}
                        position={"right"}
                    />
                </>
            )}
        </>
    ),
});
