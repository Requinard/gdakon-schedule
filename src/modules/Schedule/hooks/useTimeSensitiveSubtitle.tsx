import { match, P } from "ts-pattern";
import { Dayjs } from "dayjs";

import { useNow } from "~modules/Dev";

import { EventScheduleItemModel } from "~modules/Schedule";

const isDuringEvent = (event: EventScheduleItemModel | undefined) =>
    P.when((value: Dayjs): value is Dayjs =>
        value.isBetween(event?.startDayjs, event?.endDayjs),
    );
const isBeforeEvent = (event: EventScheduleItemModel | undefined) =>
    P.when((value: Dayjs): value is Dayjs =>
        value.isBetween(
            event?.startDayjs.subtract(30, "minutes"),
            event?.startDayjs,
        ),
    );
const isDuringEvents = (events: EventScheduleItemModel[]) =>
    P.when((value: Dayjs): value is Dayjs =>
        events.some((it) => value.isBetween(it.startDayjs, it.endDayjs)),
    );
const inBetweenTimes = (startHour: number, endHour: number) =>
    P.when((value: Dayjs): value is Dayjs =>
        value.isBetween(
            value.hour(startHour).startOf("hour"),
            value.hour(endHour).startOf("hour"),
        ),
    );

const isAfterEvent = (event: EventScheduleItemModel | undefined) =>
    P.when((value: Dayjs): value is Dayjs => value.isAfter(event?.endDayjs));
export const useTimeSensitiveSubtitle = (
    events: EventScheduleItemModel[],
): string | undefined => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "useTimeSensitiveSubtitle",
    });
    const now = useNow();
    const opening = useMemo(
        () => events.find((it) => it.name.includes("Opening")),
        [events],
    );
    const closing = useMemo(
        () => events.find((it) => it.name.includes("Closing")),
        [events],
    );
    const dances = useMemo(
        () => events.filter((it) => it.name === "[DANCEFLOOR]"),
        [events],
    );

    return useMemo(
        () =>
            match(now)
                .with(isDuringEvent(opening), () => t("opening_ongoing"))
                .with(isBeforeEvent(opening), () => t("opening_starting"))
                .with(isDuringEvent(closing), () => t("closing_ongoing"))
                .with(isBeforeEvent(closing), () => t("closing_starting"))
                .with(isDuringEvents(dances), () => t("dance"))
                .with(isAfterEvent(closing), () => t("closing_ended"))
                .with(inBetweenTimes(7, 11), () => t("breakfast"))
                .with(inBetweenTimes(2, 7), () => t("rest"))
                .otherwise(() => undefined),
        [t, opening, closing, now, dances],
    );
};
