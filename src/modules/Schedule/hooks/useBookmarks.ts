import { create } from "zustand";
import { immer } from "zustand/middleware/immer";
import { createJSONStorage, persist } from "zustand/middleware";

import { BaseEventScheduleItemModel } from "~modules/Schedule";

type BookmarksState = {
    bookmarks: Record<string, boolean>;
    toggleBookmark: (event: BaseEventScheduleItemModel) => void;
};
export const useBookmarks = create<BookmarksState>()(
    persist(
        immer((setState) => ({
            bookmarks: {},
            toggleBookmark: (item) => {
                return setState((state) => {
                    if (item.id in state.bookmarks) {
                        delete state.bookmarks[item.id];
                    } else {
                        state.bookmarks[item.id] = true;
                    }
                });
            },
        })),
        {
            name: "gdakon-bookmarks",
            storage: createJSONStorage(() => localStorage),
        },
    ),
);
