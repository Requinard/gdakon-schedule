import { getRouteApi } from "@tanstack/react-router";
import dayjs from "dayjs";
import Fuse from "fuse.js";

import { uniq } from "lodash";
import { EventScheduleItemModel, useBookmarks } from "~modules/Schedule";

const routeApi = getRouteApi("/schedule");

const getDefaultAnswer = (events: EventScheduleItemModel[]) =>
    events.map((it, i) => ({
        refIndex: i,
        item: it,
    }));

export const useEventSearch = (
    events: EventScheduleItemModel[],
): Set<string> => {
    const params = routeApi.useSearch();
    const bookmarks = useBookmarks((it) => it.bookmarks);
    const defaultEvents = useMemo(() => getDefaultAnswer(events), [events]);
    const searchBackend = useMemo(
        () =>
            new Fuse(events, {
                includeScore: true,
                isCaseSensitive: false,
                keys: ["name", "namePl", "desc", "descPl", "organizers"],
            }),
        [events],
    );

    const debouncedParams = useDebounce(params, { wait: 500 });

    const queriedItems = useMemo(() => {
        let queriedItems =
            debouncedParams.query?.length && debouncedParams.query.length > 2
                ? searchBackend.search(debouncedParams.query)
                : defaultEvents;

        if (debouncedParams.bookmarked !== undefined) {
            queriedItems = queriedItems.filter((it) => {
                const isBookmarked = it.item.id in bookmarks;
                return debouncedParams.bookmarked
                    ? isBookmarked
                    : !isBookmarked;
            });
        }

        if (debouncedParams.room !== undefined) {
            queriedItems = queriedItems.filter(
                (it) => it.item.roomName === debouncedParams.room,
            );
        }

        if (debouncedParams.date !== undefined) {
            const day = dayjs(debouncedParams.date, "DD-MM-YYYY");
            queriedItems = queriedItems.filter((it) =>
                it.item.startDayjs.isSame(day, "day"),
            );
        }

        const itemIds = new Set(queriedItems.map((it) => it.item.id));
        const dayIds = new Set(
            uniq(
                queriedItems.map((it) => it.item.groupingDayjs.format("dddd")),
            ),
        );

        return dayIds.union(itemIds);
    }, [debouncedParams, defaultEvents, searchBackend, bookmarks]);

    useDebugValue(queriedItems);

    return queriedItems;
};
