import { useNow } from "~modules/Dev";

import { EventScheduleItemModel } from "~modules/Schedule";

export const useCurrentEvents = (
    events: EventScheduleItemModel[],
): EventScheduleItemModel[] => {
    const now = useNow();

    return useMemo(
        () => events.filter((it) => now.isBetween(it.startDayjs, it.endDayjs)),
        [events, now],
    );
};
