import { useNow } from "~modules/Dev";

import { EventScheduleItemModel } from "~modules/Schedule";

export const useUpcomingEvents = (
    events: EventScheduleItemModel[],
): EventScheduleItemModel[] => {
    const now = useNow();
    return useMemo(
        () =>
            events.filter((it) =>
                it.startDayjs.isBetween(now, now.add(1, "hour")),
            ),
        [events, now],
    );
};
