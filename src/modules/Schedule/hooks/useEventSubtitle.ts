import { useMemo } from "react";
import { match } from "ts-pattern";
import { useTranslation } from "react-i18next";

import { BaseEventScheduleItemModel } from "../models";
import { useEventState } from "./useEventState";
import { useNow } from "~modules/Dev";

export const useEventSubtitle = (
    event: BaseEventScheduleItemModel,
    room: string | null,
) => {
    const now = useNow();
    const state = useEventState(event);
    const { t } = useTranslation("Main", { keyPrefix: "EventSchedule" });
    return useMemo(() => {
        const timestamp = match(state)
            .with("upcoming", () =>
                t("useEventSubtitle.starting", {
                    start: event.start,
                    time: now.to(event.startTime),
                }),
            )
            .with("current", () => t("useEventSubtitle.taking_place"))
            .otherwise(() => undefined);

        if (timestamp === undefined) {
            return null;
        }
        if (room !== null) {
            return [timestamp, room].join(" - ");
        }

        return timestamp;
    }, [now, event, state]);
};
