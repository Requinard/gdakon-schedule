import { z } from "zod";
import dayjs, { Dayjs } from "dayjs";

import { BaseEventScheduleItemModel } from "~modules/Schedule";

/**
 * Some events are after midnight, technically making them the next day. If the event starts before 03:00, just return the previous day as a grouping key
 * @param item
 */
export const getScheduleItemGroupingDay = (item: Dayjs) =>
    item.hour() > 4 ? item : item.subtract(1, "day");

export const EventScheduleItemModel = BaseEventScheduleItemModel.transform(
    (it) => {
        const start = dayjs(it.startTime);
        return {
            ...it,
            startDayjs: start,
            endDayjs: dayjs(it.endTime),
            groupingDayjs: getScheduleItemGroupingDay(start).startOf("day"),
        };
    },
);

export type EventScheduleItemModel = z.infer<typeof EventScheduleItemModel>;
