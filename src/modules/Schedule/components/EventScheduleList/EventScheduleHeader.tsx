import { Typography } from "@mui/material";
import { capitalize } from "lodash";
import { Dayjs } from "dayjs";
import { getScheduleItemGroupingDay } from "../../models";
import { Transparent } from "~modules/Common";

type EventScheduleHeaderType = {
    type: "section";
    key: string;
    date: Dayjs;
};

export type EventScheduleHeaderProps = {
    header: EventScheduleHeaderType;
    now: Dayjs;
};
export const EventScheduleHeader = ({
    header,
    now,
}: EventScheduleHeaderProps) => {
    return (
        <Transparent
            opacity={
                getScheduleItemGroupingDay(now).isAfter(header.date, "day")
                    ? 0.5
                    : 1
            }
        >
            <Typography
                variant={"h2"}
                fontWeight={400}
                data-timestamp={header.date.toISOString()}
                sx={{ px: 3 }}
            >
                {capitalize(header.date.format("dddd"))}
            </Typography>
        </Transparent>
    );
};
