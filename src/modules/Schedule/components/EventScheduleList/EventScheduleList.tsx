import { PropsWithChildren } from "react";
import { Box, Collapse, Fab, styled, useTheme, Zoom } from "@mui/material";
import { chain, partition } from "lodash";
import dayjs, { Dayjs } from "dayjs";
import { match, P } from "ts-pattern";
import { useSearch } from "@tanstack/react-router";

import { MemoizedEventCard } from "../EventCard/EventCard";

import { useNow } from "../../../Dev";
import { EventScheduleHeader } from "./EventScheduleHeader";
import {
    EventScheduleItemModel,
    getScheduleItemGroupingDay,
} from "~modules/Schedule";
import NavigateIcon from "~icons/mdi/view-day-outline";
import { NoDataIndicator, Transparent } from "~modules/Common";

export type EventScheduleListProps = {
    events: EventScheduleItemModel[];
    visible: Set<string>;
};

const HeaderCollapse = styled(Collapse)(({ theme, in: _in }) => ({
    columnSpan: "all",
    marginTop: _in ? theme.spacing(2) : 0,
    marginBottom: _in ? theme.spacing(2) : 0,
    marginLeft: theme.spacing(-3),
    marginRight: theme.spacing(-3),
    borderBottom: `${theme.palette.text.secondary} 1px solid`,
}));

type ScheduleListItem =
    | EventScheduleItemModel
    | {
          type: "section";
          key: string;
          date: Dayjs;
      };

const TIMEOUT = 750;

export const EventScheduleList = ({
    events,
    visible,
}: PropsWithChildren<EventScheduleListProps>) => {
    const now = useNow();
    const params = useSearch({ strict: false });
    const scroller = useRef(document.getElementById("scroller-element"));

    const todayScrollOffset = useMemo(() => {
        const day = getScheduleItemGroupingDay(now)
            .startOf("day")
            .toISOString();
        for (const element of document.getElementsByTagName("h2")) {
            if (element.getAttribute("data-timestamp") === day) {
                return element.offsetTop;
            }
        }

        return 0;
    }, [now]);

    console.debug(todayScrollOffset);

    const eventGroups = useMemo((): ScheduleListItem[] => {
        return chain(events)
            .orderBy((it) => it.startTime)
            .groupBy((it) => it.groupingDayjs)
            .flatMap((items, date): ScheduleListItem[] => {
                const [active, expired] = partition(items, (it) =>
                    now.isBefore(it.endDayjs),
                );
                return [
                    {
                        type: "section",
                        date: dayjs(date),
                        key: dayjs(date).startOf("day").format("dddd"),
                    },
                    ...active,
                    ...expired,
                ];
            })
            .value();
    }, [events, now]);

    const navigateToActive = useCallback(() => {
        return scroller.current?.scrollTo({
            top: todayScrollOffset ?? 0,
            behavior: "smooth",
        });
    }, [scroller, todayScrollOffset]);

    useMount(() => {
        setTimeout(() => navigateToActive(), 700);
        scroller.current = document.getElementById("scroller-element");
    });

    const debouncedParams = useDebounce(params);

    useUpdateEffect(() => {
        navigateToActive();
    }, [debouncedParams]);

    const theme = useTheme();
    const currentScroll = useScroll(scroller.current);

    const showTopScroller = useMemo(() => {
        const currentTop = currentScroll?.top;
        if (currentTop === undefined || todayScrollOffset === undefined) {
            return false;
        }

        return (
            currentTop > todayScrollOffset + 100 ||
            currentTop < todayScrollOffset - 100
        );
    }, [currentScroll, todayScrollOffset]);

    return (
        <>
            <Zoom in={showTopScroller}>
                <Fab
                    onClick={() => navigateToActive()}
                    sx={{ position: "absolute", bottom: 16, left: 16 }}
                    color={"inherit"}
                    title={"Scroll to the current day"}
                >
                    <NavigateIcon fontSize={"24px"} />
                </Fab>
            </Zoom>

            <Box
                sx={{
                    columns: "4 400px",
                    columnsGap: theme.spacing(2),
                    pb: 2,
                }}
            >
                {eventGroups.map((it) =>
                    match(it)
                        .with({ type: P.string }, (header) => (
                            <HeaderCollapse
                                key={header.key}
                                in={visible.has(header.key)}
                                timeout={TIMEOUT}
                            >
                                <EventScheduleHeader
                                    header={header}
                                    now={now}
                                />
                            </HeaderCollapse>
                        ))
                        .with({ start: P.string }, (event) => (
                            <Collapse
                                key={event.id}
                                in={visible.has(event.id)}
                                timeout={TIMEOUT}
                                sx={{
                                    mb: visible.has(event.id) ? 2 : 0,
                                }}
                            >
                                <Transparent
                                    opacity={
                                        now.isAfter(event.endDayjs) ? 0.5 : 1
                                    }
                                >
                                    <MemoizedEventCard event={event} />
                                </Transparent>
                            </Collapse>
                        ))
                        .exhaustive(),
                )}

                <Box
                    component={Collapse}
                    in={visible.size === 0}
                    sx={{ columnSpan: "all" }}
                >
                    <NoDataIndicator type={"search"} />
                </Box>
            </Box>
        </>
    );
};
