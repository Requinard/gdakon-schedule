import { getRouteApi } from "@tanstack/react-router";
import {
    Box,
    Stack,
    ToggleButton,
    ToggleButtonGroup,
    Typography,
} from "@mui/material";
import { capitalize, chain } from "lodash";
import { Dayjs } from "dayjs";

import { EventScheduleItemModel } from "~modules/Schedule";
import { useIsMobile } from "~modules/Common";

const routeInfo = getRouteApi("/schedule");
export const EventScheduleSearchDays = ({
    events,
}: {
    events: EventScheduleItemModel[];
}) => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "EventScheduleSearch.EventScheduleSearchDays",
    });
    const { date = "" } = routeInfo.useSearch();
    const navigate = routeInfo.useNavigate();
    const isSmall = useIsMobile();

    const filterToDate = useCallback(
        (newDate: Dayjs) => {
            const dateString = newDate.format("DD-MM-YYYY");
            return navigate({
                replace: true,
                search: (prev) => ({
                    ...prev,
                    date: dateString === prev.date ? undefined : dateString,
                }),
            });
        },
        [navigate],
    );

    const days = useMemo(
        () =>
            chain(events)
                .uniqBy((it) => it.startDayjs.startOf("day").format("LL"))
                .orderBy((it) => it.startDayjs.unix())
                .map((it) => it.startDayjs)
                .value(),
        [events],
    );

    return (
        <Box>
            <Typography variant={"subtitle2"}>{t("label")}</Typography>
            <ToggleButtonGroup
                value={date}
                orientation={isSmall ? "vertical" : "horizontal"}
                fullWidth
            >
                {days.map((it) => (
                    <ToggleButton
                        onClick={() => filterToDate(it)}
                        value={it.format("DD-MM-YYYY")}
                        key={it.toISOString()}
                    >
                        {capitalize(it.format("dddd"))}
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
            <Stack direction={"row"} gap={1} minWidth={0}></Stack>
        </Box>
    );
};
