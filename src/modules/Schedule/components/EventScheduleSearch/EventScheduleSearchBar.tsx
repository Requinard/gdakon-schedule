import { getRouteApi } from "@tanstack/react-router";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { isEmpty, sample } from "lodash";
import { EventScheduleItemModel } from "../../models";
import RemoveAllIcon from "~icons/mdi/text-box-remove-outline";

const routeApi = getRouteApi("/schedule");
export const EventScheduleSearchBar = ({
    events,
}: {
    events: EventScheduleItemModel[];
}) => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "EventScheduleSearch.EventScheduleSearchBar",
    });
    const params = routeApi.useSearch();
    const navigate = routeApi.useNavigate();
    const [value = "", { onChange }] = useEventTarget({
        initialValue: params.query,
    });
    const placeHolder = useMemo(() => sample(events), [events]);

    useDebounceEffect(
        () => {
            navigate({
                search: (prev) => ({
                    ...prev,
                    query: value.length > 0 ? value : undefined,
                }),
                replace: true,
            });
        },
        [value],
        { wait: 200 },
    );

    return (
        <TextField
            value={value}
            onChange={onChange}
            placeholder={t("placeholder", { item: placeHolder?.name })}
            helperText={t("helperText")}
            label={t("label")}
            slotProps={{
                input: {
                    endAdornment: (
                        <InputAdornment position={"end"}>
                            <IconButton
                                disabled={isEmpty(value)}
                                title={t("delete_tooltip")}
                                onClick={() =>
                                    onChange({ target: { value: "" } })
                                }
                                color={"error"}
                            >
                                <RemoveAllIcon />
                            </IconButton>
                        </InputAdornment>
                    ),
                },
            }}
        />
    );
};
