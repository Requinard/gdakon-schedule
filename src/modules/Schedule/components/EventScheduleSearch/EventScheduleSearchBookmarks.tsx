import { getRouteApi } from "@tanstack/react-router";
import { Checkbox, FormControlLabel } from "@mui/material";
import { match } from "ts-pattern";

const routeApi = getRouteApi("/schedule");

export const EventScheduleSearchBookmarks = () => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "EventScheduleSearch.EventScheduleSearchBookmarks",
    });
    const params = routeApi.useSearch();
    const navigate = routeApi.useNavigate();

    const cycle = useCallback(() => {
        return navigate({
            replace: true,
            search: (prev) => ({
                ...prev,
                bookmarked: match(prev.bookmarked)
                    .with(true, () => undefined)
                    .otherwise(() => true),
            }),
        });
    }, [navigate]);

    return (
        <FormControlLabel
            control={
                <Checkbox
                    indeterminate={params.bookmarked === false}
                    onClick={cycle}
                    checked={params.bookmarked === true}
                />
            }
            label={t("label")}
        />
    );
};
