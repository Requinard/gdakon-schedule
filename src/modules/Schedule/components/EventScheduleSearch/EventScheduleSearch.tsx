import {
    Avatar,
    Button,
    ButtonGroup,
    Card,
    CardContent,
    CardHeader,
    ClickAwayListener,
    Divider,
    Fab,
    Fade,
    Popper,
    Stack,
} from "@mui/material";
import {
    bindPopper,
    bindToggle,
    usePopupState,
} from "material-ui-popup-state/hooks";

import { getRouteApi, Link } from "@tanstack/react-router";
import { isEmpty, some } from "lodash";
import { EventScheduleSearchBar } from "./EventScheduleSearchBar";
import { EventScheduleSearchBookmarks } from "./EventScheduleSearchBookmarks";
import { EventScheduleSearchDays } from "./EventScheduleSearchDays";
import { EventScheduleSearchRooms } from "./EventScheduleSearchRooms";

import SearchIcon from "~icons/mdi/search";
import CloseIcon from "~icons/mdi/close";
import DeleteIcon from "~icons/mdi/delete-outline";
import { EventScheduleItemModel } from "~modules/Schedule";

const routeInfo = getRouteApi("/schedule");
export const EventScheduleSearch = ({
    events,
}: {
    events: EventScheduleItemModel[];
}) => {
    const params = routeInfo.useSearch();

    const filtersEnabled = useMemo(() => {
        return some([
            !isEmpty(params.room),
            !isEmpty(params.date),
            !isEmpty(params.query),
            !isEmpty(params.bookmarked),
        ]);
    }, [params]);
    const { t } = useTranslation("Schedule", {
        keyPrefix: "EventScheduleSearch",
    });
    const popupState = usePopupState({ variant: "popper" });

    return (
        <>
            <Popper
                placement={"top-end"}
                sx={{ pb: 2 }}
                modifiers={[]}
                {...bindPopper(popupState)}
                transition
            >
                {({ TransitionProps }) => (
                    <ClickAwayListener
                        onClickAway={() => popupState.close(null)}
                    >
                        <Fade {...TransitionProps} timeout={350}>
                            <Card
                                elevation={8}
                                variant={"elevation"}
                                sx={{ maxWidth: "95%" }}
                            >
                                <CardHeader
                                    avatar={
                                        <Avatar color={"text"}>
                                            <SearchIcon fontSize={"24px"} />
                                        </Avatar>
                                    }
                                    title={t("title")}
                                    subheader={t("subheader")}
                                    action={
                                        <ButtonGroup
                                            variant={"text"}
                                            color={"inherit"}
                                        >
                                            <Button
                                                color={"warning"}
                                                disabled={!filtersEnabled}
                                                component={Link}
                                                to={"/schedule"}
                                            >
                                                <DeleteIcon fontSize={"24px"} />
                                            </Button>
                                            <Button {...bindToggle(popupState)}>
                                                <CloseIcon fontSize={"24px"} />
                                            </Button>
                                        </ButtonGroup>
                                    }
                                />
                                <Divider />
                                <CardContent
                                    component={Stack}
                                    spacing={2}
                                    flexDirection={"column"}
                                    maxHeight={"500px"}
                                    sx={{
                                        overflowY: "auto",
                                    }}
                                >
                                    <EventScheduleSearchBar events={events} />
                                    <EventScheduleSearchDays events={events} />
                                    <EventScheduleSearchRooms events={events} />
                                    <EventScheduleSearchBookmarks />
                                </CardContent>
                            </Card>
                        </Fade>
                    </ClickAwayListener>
                )}
            </Popper>
            <Fab
                sx={{
                    position: "absolute",
                    bottom: 16,
                    right: 16,
                }}
                color={"inherit"}
                {...bindToggle(popupState)}
            >
                <SearchIcon fontSize={"24px"} />
            </Fab>
        </>
    );
};
