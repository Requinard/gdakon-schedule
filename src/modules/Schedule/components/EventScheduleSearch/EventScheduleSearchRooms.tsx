import { getRouteApi } from "@tanstack/react-router";
import {
    Box,
    Stack,
    ToggleButton,
    ToggleButtonGroup,
    Typography,
} from "@mui/material";
import { chain } from "lodash";

import { EventScheduleItemModel } from "~modules/Schedule";
import { useIsMobile } from "~modules/Common";

const routeInfo = getRouteApi("/schedule");
export const EventScheduleSearchRooms = ({
    events,
}: {
    events: EventScheduleItemModel[];
}) => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "EventScheduleSearch.EventScheduleSearchRooms",
    });
    const { room = "" } = routeInfo.useSearch();
    const navigate = routeInfo.useNavigate();
    const isSmall = useIsMobile();

    const filterToDate = useCallback(
        (newRoom: string) => {
            return navigate({
                replace: true,
                search: (prev) => ({
                    ...prev,
                    room: newRoom === prev.room ? undefined : newRoom,
                }),
            });
        },
        [navigate],
    );

    const rooms = useMemo(
        () =>
            chain(events)
                .uniqBy((it) => it.roomName)
                .map((it) => it.roomName)
                .compact()
                .sort()
                .value(),
        [events],
    );

    return (
        <Box>
            <Typography variant={"subtitle2"}>{t("label")}</Typography>
            <ToggleButtonGroup
                value={room}
                orientation={isSmall ? "vertical" : "horizontal"}
                fullWidth
            >
                {rooms.map((it) => (
                    <ToggleButton
                        onClick={() => filterToDate(it)}
                        value={it}
                        key={it}
                    >
                        {it}
                    </ToggleButton>
                ))}
            </ToggleButtonGroup>
            <Stack direction={"row"} gap={1} minWidth={0}></Stack>
        </Box>
    );
};
