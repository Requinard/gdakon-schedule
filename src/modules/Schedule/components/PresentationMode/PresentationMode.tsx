import {
    Collapse,
    lighten,
    List,
    ListItem,
    ListItemText,
    Paper,
    Stack,
    styled,
    useMediaQuery,
} from "@mui/material";
import { TransitionGroup } from "react-transition-group";

import { useCurrentEvents } from "../../hooks/useCurrentEvents";

import { useNow } from "../../../Dev";
import { PresentationModeListItem } from "./PresentationModeListItem";
import { PresentationModeClock } from "./PresentationModeClock";

import { EventScheduleItemModel, useUpcomingEvents } from "~modules/Schedule";
import { ContainerWithBackground, NoDataIndicator } from "~modules/Common";

export type PresentationModeProps = {
    events: EventScheduleItemModel[];
};

const ListItemHeader = styled(ListItem)(({ theme }) => ({
    backgroundColor: lighten(theme.palette.background.paper, 0.1),
    borderBottom: `${theme.palette.divider} 1px solid`,
}));

export const PresentationMode = ({ events }: PresentationModeProps) => {
    const { t } = useTranslation("Main", { keyPrefix: "EventSchedule" });

    const now = useNow();
    const currentEvents = useCurrentEvents(events);
    const upcomingEvents = useUpcomingEvents(events);

    const empty = useMemo(
        () => currentEvents.length + upcomingEvents.length === 0,
        [upcomingEvents, currentEvents],
    );

    const isSmall = useMediaQuery((theme) => theme.breakpoints.down("sm"));

    return (
        <ContainerWithBackground
            sx={{
                py: 2,
                display: "flex",
                flexDirection: "column",
                height: "100%",
                maxHeight: "100%",
            }}
        >
            <Stack spacing={2} alignItems={"stretch"} flex={1}>
                {!isSmall && <PresentationModeClock events={events} />}

                {!empty && (
                    <List component={Paper} sx={{ pt: 0 }}>
                        <TransitionGroup>
                            {upcomingEvents.length > 0 && (
                                <Collapse key={"upcoming header"}>
                                    <ListItemHeader>
                                        <ListItemText
                                            primary={t(
                                                "Overview.upcoming.title",
                                            )}
                                            secondary={t(
                                                "Overview.upcoming.subtitle",
                                                {
                                                    count: upcomingEvents.length,
                                                },
                                            )}
                                            slotProps={{
                                                primary: { variant: "h2" },
                                            }}
                                        />
                                    </ListItemHeader>
                                </Collapse>
                            )}
                            {upcomingEvents.map((it) => (
                                <Collapse key={it.id}>
                                    <PresentationModeListItem
                                        event={it}
                                        now={now}
                                    />
                                </Collapse>
                            ))}

                            {currentEvents.length > 0 && (
                                <Collapse key={"current ehader"}>
                                    <ListItemHeader>
                                        <ListItemText
                                            primary={t(
                                                "Overview.current.title",
                                            )}
                                            secondary={t(
                                                "Overview.current.subtitle",
                                                {
                                                    count: currentEvents.length,
                                                },
                                            )}
                                            slotProps={{
                                                primary: { variant: "h2" },
                                            }}
                                        />
                                    </ListItemHeader>
                                </Collapse>
                            )}
                            {currentEvents.map((it) => (
                                <Collapse key={it.id}>
                                    <PresentationModeListItem
                                        event={it}
                                        now={now}
                                    />
                                </Collapse>
                            ))}
                        </TransitionGroup>
                    </List>
                )}

                {empty && (
                    <Paper sx={{ py: 2 }}>
                        <NoDataIndicator />
                    </Paper>
                )}
            </Stack>
        </ContainerWithBackground>
    );
};
