import { ListItem, ListItemText, Typography } from "@mui/material";
import { Dayjs } from "dayjs";

import { EventScheduleItemModel } from "~modules/Schedule";

type PresentationModeListItemProps = {
    event: EventScheduleItemModel;
    now: Dayjs;
};
export const PresentationModeListItem = ({
    event,
    now,
}: PresentationModeListItemProps) => {
    const { t } = useTranslation("Main", { keyPrefix: "EventSchedule" });
    const upcoming = useMemo(
        () => event.startDayjs.isBetween(now, now.add(1, "hour")),
        [event, now],
    );

    const secondaryText = useMemo(() => {
        if (upcoming) {
            return t("useEventSubtitle.starting", {
                start: event.start,
                time: now.to(event.startDayjs),
            });
        } else {
            return `Until ${event.end}`;
        }
    }, [now, event, t, upcoming]);
    return (
        <ListItem key={event.id}>
            <ListItemText
                primary={event.name}
                secondary={event.roomName}
                sx={{ flex: 1 }}
                slotProps={{
                    primary: {
                        variant: "h3",
                    },
                    secondary: { variant: "h4" },
                }}
            />
            <Typography variant={"h4"}>{secondaryText}</Typography>
        </ListItem>
    );
};
