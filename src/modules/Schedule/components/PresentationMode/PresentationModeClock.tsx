import { Paper, Stack, Typography, Box } from "@mui/material";

import { useTimeSensitiveSubtitle } from "../../hooks/useTimeSensitiveSubtitle";
import { Logo } from "../../../../components/images/Logo";
import { useNow } from "../../../Dev";

import { EventScheduleItemModel } from "~modules/Schedule";

export const PresentationModeClock = ({
    events,
}: {
    events: EventScheduleItemModel[];
}) => {
    const { t } = useTranslation("Schedule", {
        keyPrefix: "PresentationMode.PresentationModeClock",
    });
    const now = useNow();

    const subtitle = useTimeSensitiveSubtitle(events);

    return (
        <Paper
            component={Stack}
            direction={"row"}
            spacing={2}
            alignItems={"center"}
            justifyContent={"space-around"}
            p={2}
        >
            <Box width={"400px"}>
                <Logo />
            </Box>
            <Stack spacing={2} alignItems={"center"} py={2}>
                <Typography variant={"h4"} fontStyle={"bold"} fontWeight={700}>
                    {t("header")}
                </Typography>
                <Typography variant={"h1"} fontWeight={700}>
                    {now.format("LT")}
                </Typography>
                {import.meta.env.DEV && (
                    <Typography variant={"h4"}>{subtitle}</Typography>
                )}
            </Stack>
        </Paper>
    );
};
