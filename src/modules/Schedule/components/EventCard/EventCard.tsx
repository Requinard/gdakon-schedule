import {
    Card,
    CardContent,
    CardHeader,
    Divider,
    Grid2,
    IconButton,
    Stack,
    styled,
    Typography,
} from "@mui/material";

import { identity } from "lodash";
import {
    EventScheduleItemModel,
    useBookmarks,
    useEventSubtitle,
    useLocalizedEvent,
} from "~modules/Schedule";
import AddBookmarkIcon from "~icons/mdi/bookmark-outline";
import BookmarkIcon from "~icons/mdi/bookmark";
import RoomIcon from "~icons/ic/baseline-room";
import PawIcon from "~icons/mdi/paw";
import EventStartIcon from "~icons/mdi/clock-start";
import EventStopIcon from "~icons/mdi/clock-end";
import DurationIcon from "~icons/mdi/timer";
import DayIcon from "~icons/mdi/calendar-today";

type EventScheduleItemCardProps = {
    event: EventScheduleItemModel;
};

const GridRow = styled(Grid2)(({ theme }) => ({
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    gap: theme.spacing(2),
}));

export const EventCard = ({ event }: EventScheduleItemCardProps) => {
    const navigate = useNavigate({ from: "/schedule" });
    const { t } = useTranslation("Schedule", { keyPrefix: "EventCard" });
    const { bookmarks, toggleBookmark } = useBookmarks();
    const { name, description, room } = useLocalizedEvent(event);
    const isBookmarked = useMemo(() => {
        return bookmarks[event.id] !== undefined;
    }, [event, bookmarks]);

    const subheader = useEventSubtitle(event, room);

    return (
        <Card variant={"elevation"}>
            <CardHeader
                title={name}
                onDoubleClick={() =>
                    navigate({
                        to: "/schedule/$id",
                        params: { id: event.id },
                        search: identity,
                    })
                }
                subheader={subheader}
                action={
                    <IconButton
                        onClick={() => toggleBookmark(event)}
                        color={isBookmarked ? "warning" : undefined}
                    >
                        {isBookmarked ? (
                            <BookmarkIcon style={{ fontSize: "2rem" }} />
                        ) : (
                            <AddBookmarkIcon style={{ fontSize: "2rem" }} />
                        )}
                    </IconButton>
                }
            />
            <Divider />
            <Grid2 container px={2} py={1} component={CardContent}>
                <GridRow size={12}>
                    <RoomIcon />
                    <Typography variant={"subtitle2"}>
                        {t("location", { room: room })}
                    </Typography>
                </GridRow>
                <GridRow size={12}>
                    <DayIcon />
                    <Typography variant={"subtitle2"}>
                        {t("day", { day: event.startDayjs.format("dddd") })}
                    </Typography>
                </GridRow>
                <GridRow size={4} title={event.startTime}>
                    <EventStartIcon />
                    <Typography variant={"subtitle2"}>
                        {t("start", { start: event.start })}
                    </Typography>
                </GridRow>
                <GridRow size={4} title={event.endTime}>
                    <EventStopIcon />{" "}
                    <Typography variant={"subtitle2"}>
                        {t("end", { end: event.end })}
                    </Typography>
                </GridRow>
                <GridRow size={4}>
                    <DurationIcon />{" "}
                    <Typography variant={"subtitle2"}>
                        {t("duration", {
                            duration: event.startDayjs.to(event.endDayjs, true),
                        })}
                    </Typography>
                </GridRow>
                {event.organizers.length > 0 && (
                    <GridRow>
                        <PawIcon />
                        <Typography variant={"subtitle2"}>
                            {t("organizers", {
                                organizers: event.organizers.join(", "),
                            })}
                        </Typography>
                    </GridRow>
                )}
            </Grid2>
            {description && (
                <>
                    <Divider />
                    <CardContent component={Stack} spacing={1}>
                        <Typography>{description}</Typography>
                    </CardContent>
                </>
            )}
        </Card>
    );
};

export const MemoizedEventCard = memo(EventCard);
