import { createJSONStorage, persist } from "zustand/middleware";
import { immer } from "zustand/middleware/immer";
import { create } from "zustand";

type TimeTravelState = {
    enabled: boolean;
    amount: number;
    toggle: () => void;
    updateAmount: (newAmount: number) => void;
};

export const useTimeTravel = create<TimeTravelState>()(
    persist(
        immer<TimeTravelState>((setState) => ({
            enabled: false,
            amount: 0,
            toggle: () =>
                setState((draft) => {
                    draft.enabled = !draft.enabled;
                }),
            updateAmount: (newAmount) =>
                setState((draft) => {
                    draft.amount = newAmount;
                }),
        })),
        {
            name: "gdakon_time_travel_state",
            storage: createJSONStorage(() => localStorage),
        },
    ),
);
