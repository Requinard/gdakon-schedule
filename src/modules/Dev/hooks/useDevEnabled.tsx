export const useDevEnabled = (): [boolean, () => void] => {
    const clickRef = useRef(0);
    const [devEnabled = false, setDevEnabled] = useLocalStorageState<boolean>(
        "gdakon-dev-enabled",
        {
            defaultValue: () => false,
            listenStorageChange: true,
        },
    );

    const clickHandler = useCallback(() => {
        if (devEnabled) {
            return;
        }
        clickRef.current = clickRef.current + 1;

        if (clickRef.current >= 5) {
            alert("Dev mode is enabled! good luck");
            setDevEnabled(true);
        }
    }, [devEnabled, clickRef, setDevEnabled]);

    return [devEnabled, clickHandler];
};
