import dayjs, { Dayjs } from "dayjs";

import { useTimeTravel } from "../index";

import { useLocale } from "~i18n";

const calculateNow = (
    enabled: boolean,
    amount: number,
    locale: string,
): Dayjs =>
    enabled
        ? dayjs().locale(locale).add(amount, "milliseconds")
        : dayjs().locale(locale);

export const useNow = (): Dayjs => {
    const { locale } = useLocale();
    const { enabled, amount } = useTimeTravel();

    const [now, setNow] = useState(calculateNow(enabled, amount, locale));

    useEffect(() => {
        setNow(calculateNow(enabled, amount, "en-gb"));
    }, [setNow, enabled, amount, locale]);

    useInterval(() => {
        const newNow = calculateNow(enabled, amount, "en-gb");

        setNow(newNow);
    }, 10_000);

    return now;
};
