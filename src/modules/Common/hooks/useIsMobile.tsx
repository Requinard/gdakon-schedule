export const useIsMobile = (): boolean => {
    return useMemo(() => {
        return screen.height / screen.width > 1;
    }, []);
};
