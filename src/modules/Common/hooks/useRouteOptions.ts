import { useRouter } from "@tanstack/react-router";
import type { RouteOptions } from "@tanstack/react-router";

export const useRouteOptions = (to: any) => {
    const router = useRouter();

    const routeOpts = useMemo(
        // @ts-expect-error Yeah this kinda breaks but I promise it works!
        (): RouteOptions | undefined => router.routesByPath[to]?.options,
        [router, to],
    );

    useDebugValue(routeOpts);

    return routeOpts;
};
