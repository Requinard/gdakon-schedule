import { useRouteOptions } from "~modules/Common";

export const useRouteIcon = (to: any) => {
    const opts = useRouteOptions(to);

    return useMemo(() => opts?.staticData?.icon, [opts]);
};
