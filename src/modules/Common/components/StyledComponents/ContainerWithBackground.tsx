import { styled, Box, Container } from "@mui/material";
import { ContainerProps } from "@mui/system";
import BackgroundImage from "../../../../assets/steam powered banner 2025_06_CROP_01J.jpg?blur=2&url&format=webp&imagetools";

const Outer = styled(Box)({
    backgroundImage: `url(${BackgroundImage})`,
    backgroundSize: "cover",
    width: "100%",
    maxWidth: "100%",
    overflowX: "clip",
    maxHeight: "100%",
    height: "100%",
    overflowY: "auto",
});

export const ContainerWithBackground = (props: ContainerProps) => (
    <Outer id={"scroller-element"}>
        <Container {...props} />
    </Outer>
);
