import { PropsWithChildren } from "react";
import { animated, config, useSpring } from "@react-spring/web";
import { useHover } from "ahooks";

type TransparentProps = {
    opacity?: number;
};

export const Transparent = ({
    children,
    opacity = 1,
}: PropsWithChildren<TransparentProps>) => {
    const ref = useRef(null);
    const [props, api] = useSpring(() => ({
        from: {
            opacity: opacity,
        },

        config: config["stiff"],
    }));

    useHover(ref, {
        onEnter: () => {
            api.start({ opacity: 1 });
        },
        onLeave: () => {
            api.start({ opacity: opacity });
        },
    });

    return (
        <animated.div style={props} ref={ref}>
            {children}
        </animated.div>
    );
};
