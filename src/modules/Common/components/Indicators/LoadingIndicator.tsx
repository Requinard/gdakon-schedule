import {
    Box,
    CircularProgress,
    Stack,
    Typography,
    Zoom,
    Paper,
    useTheme,
} from "@mui/material";
export const LoadingIndicator = () => {
    const { t } = useTranslation("Main", { keyPrefix: "LoadingIndicator" });
    const { spacing } = useTheme();
    return (
        <Box
            display={"flex"}
            flex={1}
            alignItems={"center"}
            justifyContent={"center"}
            flexDirection={"column"}
        >
            <Zoom in>
                <Paper
                    elevation={3}
                    component={Stack}
                    gap={4}
                    alignItems={"center"}
                    p={4}
                    sx={{ borderRadius: spacing(2) }}
                >
                    <CircularProgress size={120} />
                    <Typography variant={"h5"}>{t("loading")}</Typography>
                </Paper>
            </Zoom>
        </Box>
    );
};
