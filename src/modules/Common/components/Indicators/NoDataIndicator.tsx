import { Button, Stack, Typography } from "@mui/material";
import { useRouter } from "@tanstack/react-router";
import SadGdakonek from "../../../../assets/gdakonek/sad.webp?h=256&imagetools";

export type NoDataIndicatorProps = {
    type?: "page" | "search";
};

export const NoDataIndicator = ({ type }: NoDataIndicatorProps) => {
    const router = useRouter();
    const { t } = useTranslation("Common", {
        keyPrefix: "Indicators.NoDataIndicator",
    });

    const text = useMemo(
        () =>
            match(type)
                .with("page", () => t("page_not_found"))
                .with("search", () => t("search_no_results"))
                .with(undefined, () => t("regular"))
                .exhaustive(),
        [type, t],
    );
    return (
        <Stack
            spacing={2}
            flex={1}
            alignItems={"center"}
            justifyContent={"center"}
            height={"100%"}
            flexDirection={"column"}
            maxHeight={"100%"}
            py={2}
        >
            <img
                src={SadGdakonek}
                alt={
                    "Gdakonek is looking really sad because the page/data you are looking for does not exist"
                }
            />
            <Typography variant={"h4"}>{text}</Typography>
            {type === "page" && (
                <Button
                    onClick={() =>
                        router.history.canGoBack()
                            ? router.history.back()
                            : router.navigate({ to: "/" })
                    }
                >
                    {router.history.canGoBack()
                        ? t("button_previous")
                        : t("button_home")}
                </Button>
            )}
        </Stack>
    );
};
