import { BottomNavigationAction } from "@mui/material";
import type { BottomNavigationActionProps } from "@mui/material";
import { createLink } from "@tanstack/react-router";
import type { LinkComponentProps } from "@tanstack/react-router";

import { useRouteIcon, useRouteOptions } from "../../hooks";

type MUIBottomNavigationLinkProps = BottomNavigationActionProps;

const MUIBottomNavigationLinkComponent = forwardRef<
    HTMLButtonElement,
    MUIBottomNavigationLinkProps
>((props, ref) => <BottomNavigationAction ref={ref} {...props} />);

const CreatedMUIBottomNavigationLink = createLink(
    MUIBottomNavigationLinkComponent,
);

type CreatedMUIBottomNavigationLinkFullProps = LinkComponentProps<
    typeof MUIBottomNavigationLinkComponent
>;

type CreatedMUIBottomNavigationLinkMapped = Omit<
    CreatedMUIBottomNavigationLinkFullProps,
    "value" | "to"
> & {
    value: CreatedMUIBottomNavigationLinkFullProps["to"];
};

export const MUIBottomNavigationLink = ({
    value,
    ...props
}: CreatedMUIBottomNavigationLinkMapped) => {
    const NavIcon = useRouteIcon(value);

    const opts = useRouteOptions(value);

    return (
        <CreatedMUIBottomNavigationLink
            preload={"intent"}
            icon={NavIcon && <NavIcon />}
            value={value}
            to={value}
            label={opts?.staticData?.name}
            {...props}
        />
    );
};
