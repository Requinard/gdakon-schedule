import { Box, Divider } from "@mui/material";
import { PropsWithChildren } from "react";

import { AppNavigationBar } from "./AppNavigationBar";

export const Dashboard = ({ children }: PropsWithChildren) => {
    return (
        <>
            <Box
                position={"absolute"}
                top={0}
                bottom={0}
                left={0}
                right={0}
                display={"flex"}
                flexDirection={"column"}
            >
                <Box
                    flexGrow={1}
                    maxHeight={"100%"}
                    minHeight={0}
                    overflow={"auto"}
                    position={"relative"}
                    display={"flex"}
                >
                    {children}
                </Box>
                <Divider />
                <AppNavigationBar />
            </Box>
        </>
    );
};
