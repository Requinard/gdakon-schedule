import { BottomNavigation, Box } from "@mui/material";
import { useLocation } from "@tanstack/react-router";

import Logo from "../../../../assets/gdakon-2025-logo.png";
import { MUIBottomNavigationLink, useIsMobile } from "~modules/Common";
import { useDevEnabled } from "~modules/Dev";

export const AppNavigationBar = () => {
    const location = useLocation();
    const isMobile = useIsMobile();
    const [dev] = useDevEnabled();

    if (location.pathname === "/presentation") {
        return undefined;
    }
    return (
        <BottomNavigation
            showLabels
            value={location.pathname}
            sx={{ position: "relative", minHeight: 56, maxHeight: 56 }}
        >
            {!isMobile && (
                <Box
                    position={"absolute"}
                    height={56}
                    bottom={0}
                    left={0}
                    pl={4}
                >
                    <img src={Logo} height={54} />
                </Box>
            )}
            <MUIBottomNavigationLink value={"/"} />
            <MUIBottomNavigationLink value={"/schedule"} />

            {dev && <MUIBottomNavigationLink value={"/presentation"} />}
            <MUIBottomNavigationLink value={"/settings"} />
        </BottomNavigation>
    );
};
