import { ListItem, ListItemText } from "@mui/material";
import { memo } from "react";

type ListSectionProps = {
    title: string;
    subtitle?: string | null;
};
export const ListSection = memo(({ title, subtitle }: ListSectionProps) => (
    <ListItem dense sx={{ bgcolor: "background.paper" }}>
        <ListItemText primary={title} secondary={subtitle} />
    </ListItem>
));
