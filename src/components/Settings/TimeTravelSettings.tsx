import {
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    ListSubheader,
    MenuItem,
    Switch,
} from "@mui/material";
import { useCallback } from "react";
import dayjs, { Dayjs } from "dayjs";
import { DateTimePicker } from "@mui/x-date-pickers";
import { useTranslation } from "react-i18next";

import { useDevEnabled, useNow, useTimeTravel } from "~modules/Dev";

const EnableTimeTravel = () => {
    const { t } = useTranslation("Main", { keyPrefix: "Settings" });
    const enabled = useTimeTravel((state) => state.enabled);
    const toggle = useTimeTravel((state) => state.toggle);

    return (
        <MenuItem onClick={toggle}>
            <ListItemText
                primary={t("TimeTravelSettings.EnableTimeTravel.title")}
                secondary={t("TimeTravelSettings.EnableTimeTravel.title")}
                secondaryTypographyProps={{
                    noWrap: true,
                }}
            />
            <ListItemSecondaryAction>
                <Switch checked={enabled} color={"secondary"} />
            </ListItemSecondaryAction>
        </MenuItem>
    );
};

const TimeTravelPicker = () => {
    const { t } = useTranslation("Main", { keyPrefix: "Settings" });
    const now = useNow();

    const updateAmount = useTimeTravel((state) => state.updateAmount);

    const changeTimeTravelAmount = useCallback(
        (value: Dayjs | null) => {
            if (value === null) {
                return;
            }
            const diff = value.diff(dayjs(), "milliseconds");

            updateAmount(diff);
        },
        [updateAmount],
    );

    return (
        <ListItem>
            <DateTimePicker
                value={now}
                onChange={changeTimeTravelAmount}
                label={t("TimeTravelSettings.TimeTravelPicker.label")}
                sx={{ my: 1 }}
                slotProps={{
                    textField: {
                        fullWidth: true,
                    },
                }}
            />
        </ListItem>
    );
};
export const TimeTravelSettings = () => {
    const { t } = useTranslation("Main", { keyPrefix: "Settings" });

    const [dev] = useDevEnabled();

    if (!dev) {
        return null;
    }
    return (
        <>
            <ListSubheader>{t("TimeTravelSettings.title")}</ListSubheader>
            <EnableTimeTravel />
            <TimeTravelPicker />
        </>
    );
};
