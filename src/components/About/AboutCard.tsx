import {
    Avatar,
    Card,
    CardHeader,
    Link,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ListSubheader,
} from "@mui/material";
import { useTranslation } from "react-i18next";

import dayjs from "dayjs";
import { LanguageSettings } from "../Settings/LanguageSettings";
import { TimeTravelSettings } from "../Settings/TimeTravelSettings";
import { OnlineCheck } from "../Home/OnlineCheck";
import { ServiceWorker } from "../Common/ServiceWorker";

import GitIcon from "~icons/mdi/git";
import BuildIcon from "~icons/mdi/build";
import RepositoryIcon from "~icons/mdi/open-in-new";
import { name, version } from "~build/package";
import buildDate from "~build/time";
import { useDevEnabled } from "~modules/Dev";

export const AboutCard = () => {
    const { t } = useTranslation("Main", { keyPrefix: "About" });
    const [, devModeHandler] = useDevEnabled();
    return (
        <Card>
            <CardHeader title={t("AboutCard.title")} />
            <ServiceWorker />
            <OnlineCheck showIfOnline />
            <List>
                <ListSubheader>
                    {t("AboutCard.BuildInfo.subheader")}
                </ListSubheader>
                <ListItem>
                    <ListItemIcon>
                        <GitIcon fontSize={"1.8rem"} />
                    </ListItemIcon>
                    <ListItemText primary={name} secondary={version} />
                </ListItem>
                <ListItem onClick={devModeHandler}>
                    <ListItemIcon>
                        <BuildIcon fontSize={"1.8rem"} />
                    </ListItemIcon>
                    <ListItemText
                        primary={dayjs(buildDate).format("LLL")}
                        secondary={"Build Timestamp"}
                    />
                </ListItem>
                <ListItem
                    component={Link}
                    href={"https://gitlab.com/Requinard/gdakon-schedule"}
                    target="_blank"
                    rel={"noopener"}
                >
                    <ListItemIcon>
                        <RepositoryIcon fontSize={"1.8rem"} />
                    </ListItemIcon>
                    <ListItemText
                        primary={t("AboutCard.BuildInfo.gitlab_primary")}
                        secondary={t("AboutCard.BuildInfo.gitlab_secondary")}
                    />
                </ListItem>
                <ListSubheader>
                    {t("AboutCard.Developers.subheader")}
                </ListSubheader>
                <ListItem
                    component={Link}
                    href={"https://requinard.omg.lol/"}
                    target={"_blank"}
                    rel={"noopener"}
                >
                    <ListItemIcon>
                        <Avatar
                            src={
                                "https://avatars.githubusercontent.com/u/5537850?v=4"
                            }
                        />
                    </ListItemIcon>

                    <ListItemText
                        primary={t("AboutCard.Developers.requinard_primary")}
                        secondary={t(
                            "AboutCard.Developers.requinard_secondary",
                        )}
                    />
                </ListItem>
                <ListItem
                    component={Link}
                    href={"https://t.me/lemurr"}
                    target={"_blank"}
                    rel={"noopener"}
                >
                    <ListItemIcon>
                        <Avatar
                            src={
                                "https://avatars.githubusercontent.com/u/19829931?v=4"
                            }
                        />
                    </ListItemIcon>

                    <ListItemText
                        primary={t("AboutCard.Developers.lemurr_primary")}
                        secondary={t("AboutCard.Developers.lemurr_secondary")}
                    />
                </ListItem>
                <LanguageSettings />
                <TimeTravelSettings />
            </List>
        </Card>
    );
};
