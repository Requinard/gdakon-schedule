import logoUrl from "../../assets/gdakon-2025-logo.png?format=webp&h=100;450&imagetools";

export const Logo = () => {
    return (
        <img
            src={logoUrl[1]}
            data-src={logoUrl[0]}
            alt={"A version of the Gdakon logo, stylized for this years theme."}
            width={"100%"}
            height={"auto"}
        />
    );
};
