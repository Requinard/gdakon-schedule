import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from "@mui/material";

import {
    useBookmarks,
    useLocalizedEvent,
    useEventSubtitle,
    BaseEventScheduleItemModel,
} from "~modules/Schedule";
import BookmarkIcon from "~icons/mdi/bookmark";

type EventListItemProps = {
    event: BaseEventScheduleItemModel;
};

export const EventListItem = ({ event }: EventListItemProps) => {
    const { name, room } = useLocalizedEvent(event);

    const isBookmarked = useBookmarks(
        (state) => state.bookmarks[event.id] !== undefined,
    );

    const subtitle = useEventSubtitle(event, room);

    return (
        <ListItem>
            <ListItemText primary={name} secondary={subtitle} />
            {isBookmarked && (
                <ListItemSecondaryAction>
                    <IconButton color={"warning"}>
                        <BookmarkIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            )}
        </ListItem>
    );
};
