import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { PropsWithChildren } from "react";
import { persistQueryClient } from "@tanstack/react-query-persist-client";
import { createSyncStoragePersister } from "@tanstack/query-sync-storage-persister";

import { sha, branch } from "~build/git";

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            staleTime: 1000 * 60 * 5, // 5 minutes
            gcTime: 1000 * 60 * 60 * 24, // 24 hours
            networkMode: "offlineFirst",
        },
    },
});

const localStoragePersister = createSyncStoragePersister({
    storage: window.localStorage,
});
// if (import.meta.env.PROD) {
persistQueryClient({
    queryClient: queryClient,
    persister: localStoragePersister,
    buster: import.meta.env.PROD ? sha : branch, // Bust cache on SHA changes for prod and branches for dev
});
// }
export const AppQueryClientProvider = ({ children }: PropsWithChildren) => (
    <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
);
