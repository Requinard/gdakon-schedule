import { createTheme, lighten } from "@mui/material";
import { green, lightBlue, orange, red } from "@mui/material/colors";

export const darkTheme = createTheme({
    palette: {
        mode: "light",
        primary: {
            main: "#191919",
            dark: "#22244b",
        },
        background: {
            paper: "#a78d73",
            default: lighten("#a78d73", 0.5),
        },
        divider: lighten("#a78d73", 0.3),
        secondary: {
            light: "#51ff53",
            main: "#1cde1e",
            dark: "#0e6f0f",
        },
        warning: { main: orange[300] },
        error: { main: red[300] },
        info: { main: lightBlue[500] },
        success: {
            main: green[400],
        },
    },
    components: {
        MuiContainer: {
            defaultProps: {
                maxWidth: "xl",
            },
        },
        MuiCardHeader: {
            defaultProps: {
                titleTypographyProps: {
                    variant: "h5",
                },
                subheaderTypographyProps: {
                    variant: "subtitle1",
                },
            },
            styleOverrides: { action: { alignSelf: "center" } },
        },
        MuiCardContent: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: lighten(
                        theme.palette.background.paper,
                        0.5,
                    ),
                }),
            },
        },
        MuiBottomNavigationAction: {
            styleOverrides: {
                root: ({ theme }) => ({
                    color: theme.palette.text.primary,
                }),
            },
        },
        MuiListItemText: {
            styleOverrides: {
                root: {
                    overflow: "hidden",
                },
            },
        },
        MuiListItem: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: theme.palette.background.default,
                }),
            },
        },
        MuiListItemButton: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: theme.palette.background.default,
                }),
            },
        },
        MuiMenuItem: {
            styleOverrides: {
                root: ({ theme }) => ({
                    backgroundColor: theme.palette.background.default,
                }),
            },
        },
        MuiAvatar: {
            styleOverrides: {
                root: {
                    backgroundColor: "rgba(0,0,0,0)",
                    color: "inherit",
                },
                colorDefault: {},
            },
        },
        MuiAlert: {
            styleOverrides: {
                root: {
                    borderRadius: 0,
                },
            },
        },
    },
});
