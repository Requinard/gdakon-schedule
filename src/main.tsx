import "@fontsource/roboto/latin.css";
import "./utilities/dayjs";

import React, { ForwardRefExoticComponent, SVGProps } from "react";
import ReactDOM from "react-dom/client";
import { CssBaseline } from "@mui/material";
import { createRouter, RouterProvider } from "@tanstack/react-router";

import { LocaleProvider } from "./i18n";
import { routeTree } from "./routeTree.gen";

import { LoadingIndicator, NoDataIndicator } from "~modules/Common";
import {
    AppQueryClientProvider,
    AppThemeProvider,
    queryClient,
} from "~providers";

const router = createRouter({
    routeTree,
    defaultPendingComponent: LoadingIndicator,
    defaultNotFoundComponent: () => <NoDataIndicator type={"page"} />,
    context: {
        queryClient: queryClient,
    },
});

declare module "@tanstack/react-router" {
    interface Register {
        router: typeof router;
    }

    interface StaticDataRouteOption {
        icon?: ForwardRefExoticComponent<SVGProps<SVGSVGElement>>;
        name?: string;
        hideNavbar?: boolean;
    }
}

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
    <React.StrictMode>
        <AppThemeProvider>
            <AppQueryClientProvider>
                <LocaleProvider>
                    <CssBaseline />
                    <RouterProvider router={router} />
                </LocaleProvider>
            </AppQueryClientProvider>
        </AppThemeProvider>
    </React.StrictMode>,
);
