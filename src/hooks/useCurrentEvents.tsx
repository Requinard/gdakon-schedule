import { useMemo } from "react";
import { chain } from "lodash";

import { useNow } from "~modules/Dev";

import { BaseEventScheduleItemModel } from "~modules/Schedule";

export const useCurrentEvents = (events: BaseEventScheduleItemModel[]) => {
    const now = useNow();

    return useMemo(
        () =>
            chain(events)
                .filter((event) =>
                    now.isBetween(event.startTime, event.endTime),
                )
                .value(),
        [events, now],
    );
};
