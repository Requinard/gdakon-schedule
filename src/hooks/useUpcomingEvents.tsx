import { useMemo } from "react";
import { chain } from "lodash";
import dayjs from "dayjs";

import { useNow } from "~modules/Dev";

import { BaseEventScheduleItemModel } from "~modules/Schedule";

export const useUpcomingEvents = (events: BaseEventScheduleItemModel[]) => {
    const now = useNow();

    return useMemo(
        () =>
            chain(events)
                .filter(
                    (event) =>
                        now.isBefore(event.startTime) &&
                        now.isAfter(
                            dayjs(event.startTime).subtract(3, "hours"),
                        ),
                )
                .filter((event) => now.isBefore(event.startTime))
                .value(),
        [events, now],
    );
};
