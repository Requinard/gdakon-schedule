// eslint-disable-next-line import-x/no-named-as-default
import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import resources from "virtual:i18next-loader";

// declare module "i18next" {
//     // Extend CustomTypeOptions
//     interface CustomTypeOptions {
//         // custom namespace type, if you changed it
//         defaultNS: "Main";
//         // custom resources type
//         resources: ResourceOptions;
//         // other
//     }
// }

i18n.use(initReactI18next).init({
    defaultNS: "Main",
    resources,
    lng: "en",
    fallbackLng: "en",
    debug: import.meta.env.DEV,
    missingKeyHandler: (key) => console.warn("missing key", key),
});
