/// <reference types="vite/client" />
declare module "*&imagetools" {
    /**
     * actual types
     * - code https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/output-formats.ts
     * - docs https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/getting-started.md#metadata
     */
    const out;

    export default out;
}
// Allow for virtual module imports
// https://vitejs.dev/guide/api-plugin.html#virtual-modules-convention
declare module "virtual:i18next-loader" {
    declare const resources: import("i18next").Resource;

    export type ResourceOptions = (typeof resources)["en"];

    export default resources;
}
