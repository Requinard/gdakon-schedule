// @ts-check

import eslint from "@eslint/js";
import tseslint from "typescript-eslint";
import react from "eslint-plugin-react";
import reactHooks from "eslint-plugin-react-hooks";
import { fixupPluginRules } from "@eslint/compat";
import i18nJson from "eslint-plugin-i18n-json";
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended'
import AutoImportRules from "./.eslintrc-auto-import.json" with {type: "json"}
import eslintPluginImportX from 'eslint-plugin-import-x'
import i18next from 'eslint-plugin-i18next';
import tsParser from '@typescript-eslint/parser'
import path from "node:path";
export default tseslint.config(
    eslint.configs.recommended,
    ...tseslint.configs.recommended,
    {
        ...eslintPluginPrettierRecommended,
        files: ["**/*.ts*", "**/*.js", "**/*.jsx"],
    },
    {
        languageOptions: {
            globals: AutoImportRules.globals
        },
        // Override for some typescript we actuall kinda like
        rules: {
            "@typescript-eslint/no-explicit-any": "off",
        },
    },
    {
        files: ["**/locales/**/*.json"],
        plugins: {
            "i18n-json": i18nJson,
        },
        processor: {
            meta: {name: ".json"},
                ...i18nJson.processors['.json']
        },
        rules: {
            ...i18nJson.configs.recommended.rules,
            "i18n-json/sorted-keys": "warn",
            "i18n-json/valid-message-syntax": "off",
            "i18n-json/identical-keys": [
                1,
                {
                    filePath: {
                        "Main.json": path.resolve("./src/i18n/locales/en/Main.json"),
                        "Schedule.json": path.resolve("./src/i18n/locales/en/Schedule.json"),
                        "Common.json": path.resolve("./src/i18n/locales/en/Common.json"),
                    }
                },
            ],
            "@typescript-eslint/no-unused-expressions": "off",
        },
    },
    {
        // Override for config js files
        files: ["**/*.config.js"],
        extends: [tseslint.configs.disableTypeChecked],
        rules: {
            "no-undef": "off",
            "@typescript-eslint/no-require-imports": "off",
        },
    },
    {
        // override for testing related stuff
        files: ["**/setupTests.js"],
        extends: [tseslint.configs.disableTypeChecked],
        rules: {
            "no-undef": "off",
        },
    },
    {
        // Handle react
        files: ["**/*.tsx", "**/*.jsx"],
        plugins: {
            react: react,
            "react-hooks": fixupPluginRules(reactHooks),
            "i18next": i18next
        },
        settings: {
            react: {
                version: "18",
            },
        },
        rules: {
            ...react.configs.recommended.rules,
            ...reactHooks.configs.recommended.rules,
            ...i18next.configs['flat/recommended'].rules,
            "react/react-in-jsx-scope": "off",
            "react/prop-types": "off",
            "react/display-name": "off",
        },
    },
    {
        files: ["**/routes/**/*"],
        rules: {
            "react-hooks/rules-of-hooks": "off"
        }
    },
    {
        // Enfore importing rules
        files: ["**/*.ts", "**/*.tsx", "**/*.js", "**/*.jsx"],
        plugins: {
            "import": eslintPluginImportX
        },
        extends: [
            eslintPluginImportX.flatConfigs.recommended,
            eslintPluginImportX.flatConfigs.typescript
        ],
        languageOptions: {
            parser: tsParser,
            ecmaVersion: "latest",
            sourceType: "module"
        },
        rules: {
            "import-x/default": "warn",
            "import-x/no-unresolved": "off",
            "import-x/namespace": "warn",
            "import-x/imports-first": "warn",
            "import-x/order": "warn",
            "import-x/no-named-as-default-member": "off"
        },
    }
);
